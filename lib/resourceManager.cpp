#include "resourceManager.h"

Aipo::ResourceManager::ResourceManager(Aipo::Client *client) : client(client) {}

/**
 *
 * @param std::string username
 * @param std::string password
 *
 * @return Aipo::Response
 *
 * @throws std::exception
 */
Aipo::Response Aipo::ResourceManager::login(utility::string_t username, utility::string_t password) {
	http_request login(http::methods::POST);
	login.set_request_uri(U("/login"));

	json::value body = json::value::object(true);
	body[U("username")] = json::value::string(username);
	body[U("password")] = json::value::string(password);

	login.set_body(body);

	return client->request(login);
}

/**
 * Retrieves available rooms.
 *
 * @return Aipo::Response
 */
Aipo::Response Aipo::ResourceManager::getAvailableRooms() {
	http_request request(http::methods::GET);

	request.set_body("");
	request.set_request_uri(U("/rooms"));

	return client->requestAuthorized(request, authorizationToken);
}

Aipo::Response Aipo::ResourceManager::getRobotCommands() {
	http_request request(http::methods::GET);

	request.set_body("");
	request.set_request_uri(U("/commands"));

	return client->requestAuthorized(request, authorizationToken);
}

Aipo::Response Aipo::ResourceManager::putDoneRobotCommand(json::value _jsonValue) {
	http_request request(http::methods::PUT);
	utility::string_t taskNr = _jsonValue.serialize();
	utility::string_t uriAdr = U("/command/") +  taskNr + U("/finish");

	request.set_request_uri(uriAdr);

	return client->requestAuthorized(request, authorizationToken);
}


/**
 * Set authorization token value.
 */
void Aipo::ResourceManager::setAuthorizationToken(const utility::string_t token) {
	authorizationToken = token;
}

/**
 * Destructor.
 */
Aipo::ResourceManager::~ResourceManager() {
}
