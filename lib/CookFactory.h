#pragma once

#include "RobotBuilder.h"
#include "RobotCook.h"

namespace Aipo
{

	class CookFactory : public RobotFactory
	{

	public:
		/**
		 * @brief 
		 * 
		 * @return Robot* 
		 */
		virtual Robot* create()
		{
			return new Cook;
		}
	};

}