#pragma once

#include "RobotBuilder.h"
#include "RobotTechnician.h"

namespace Aipo
{

	class TechnicianFactory : public RobotFactory
	{

	public:
		/**
		 * @brief 
		 * 
		 * @return Robot* 
		 */
		virtual Robot* create()
		{
			return new Technician;
		}
	};

}