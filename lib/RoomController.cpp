#include "RoomController.h"

Aipo::RoomController::RoomController(Aipo::ResourceManager *resourceManager, Aipo::IConfig *config) :
    HotelController(resourceManager, config) {}

void Aipo::RoomController::run() {
	responseToken = new Response(resourceManager->login(config->get(U("room_controller_login")), config->get(U("room_controller_password"))));

  if (!responseToken->getResponse().has_field(U("token"))) {
	  throw ERROR_API_LOGIN;
  }

  resourceManager->setAuthorizationToken(responseToken->getResponse()[U("token")].as_string());

}
