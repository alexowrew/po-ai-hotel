#include "JSONConfig.h"

using namespace Aipo;

JSONConfig::JSONConfig(utility::string_t _path)
{

	utility::ifstream_t configFile;
	// Otwieramy plik
	configFile.open(_path);

	if (!configFile.is_open())
	{
		throw ERROR_FILE_OPEN_ERROR;
	}

	// Odczytujemy plik.
	while (configFile >> fileBuffer)
	{
		file += fileBuffer;
	}

	// zamykamy plik, juz nie jest potrzebny
	configFile.close();

	// ladujemy odczytana konfiguracje do obiektu
	config = json::value::parse(file);
}

JSONConfig::~JSONConfig()
{

}

utility::string_t JSONConfig::get(utility::string_t configKey)
{
	return config[configKey].as_string();
}
