#pragma once

#include "HotelController.h"
#include <string>

namespace Aipo {
class RoomController : public HotelController {

 public:
  /**
   * @brief Construct a new Room Controller object
   * 
   * @param resourceManager 
   * @param config 
   */
  RoomController(ResourceManager *resourceManager, IConfig *config);

  /**
   * @brief 
   * 
   */
  virtual void run();
};

}

