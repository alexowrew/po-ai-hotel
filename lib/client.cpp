#include "client.h"
#include "response.h"

Aipo::Client::Client(utility::string_t config)
{
	http_client_config cfg;

	cfg.set_timeout(std::chrono::milliseconds(5000));

	client = std::make_shared<http_client>(uri(config), cfg);
}

Aipo::Response Aipo::Client::request(http_request &request) {
	try {
		pplx::task<http_response> requestTask = client->request(request).then([=](http_response response) {
		  return response;
		});

		requestTask.wait();

		http_response response = requestTask.get();

		return Aipo::Response(response.status_code(), response.extract_json(true).get());
	}
	catch (const pplx::task_canceled &e) {
		return Aipo::Response(500, web::json::value());
	}
	catch (const std::exception &e) {
		// log error?
		std::cout << "Err: "  << e.what() << std::endl;
//		throw e;
		return Aipo::Response(500, web::json::value());
	}
}

Aipo::Response Aipo::Client::requestAuthorized(http_request &request, utility::string_t &authorizationToken) {
	utility::string_t str = U("Bearer ");
	str.append(authorizationToken);
	request.headers()[U("Authorization")] = str;

	return this->request(request);
}

Aipo::Client::~Client() {
}
