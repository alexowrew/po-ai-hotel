#pragma once

#include "Robot.h"
#include <vector>


namespace Aipo
{
	class Cook : public Robot
	{
	public:
		/**
		 * @brief Construct a new Cook object
		 * 
		 */
		Cook();

		/**
		 * @brief Construct a new Cook object
		 * 
		 * @param _robotNumber 
		 */
		Cook(int _robotNumber);


		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 * @param _resourceManager 
		 */
		virtual void order(web::json::value _jsonTask);
	};
}