#pragma once

// vendor libraries
#include <cpprest/http_client.h>

#include "definitions.h"

using namespace web;

namespace Aipo
{
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	// UWAGA! W calej aplikacji uzywamy web::utility::string_t z paczki cpprestsdk!
	// Zapomnijcie o std::string !
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	class IConfig
	{
	protected:
		utility::string_t file;
		utility::string_t fileBuffer;

	public:
		/**
		 * @brief Construct a new IConfig object
		 * 
		 */
		IConfig();

		/**
		 * @brief Destroy the IConfig object
		 * 
		 */
		virtual ~IConfig();

		/**
		 * @brief 
		 * 
		 * @param configKey 
		 * @return utility::string_t 
		 */
		virtual utility::string_t get(utility::string_t configKey) = 0;
	};
}
