#include "RobotController.h"

using namespace Aipo;

RobotController::RobotController(Aipo::ResourceManager *resourceManager, Aipo::IConfig *config) :
	HotelController(resourceManager, config)
{
	responseToken = new Response(resourceManager->login(config->get(U("robot_controller_login")), config->get(U("robot_controller_password"))));

	if (!responseToken->getResponse().has_field(U("token"))) {
		throw ERROR_API_LOGIN;
	}

	resourceManager->setAuthorizationToken(responseToken->getResponse()[U("token")].as_string());

	RobotFactory *_robotFactory = nullptr;

	MaidFactory _maidFactory;
	_robotFactory = &_maidFactory;

	add(_robotFactory->create());
	add(_robotFactory->create());

	CookFactory _cookFactory;
	_robotFactory = &_cookFactory;

	add(_robotFactory->create());
	add(_robotFactory->create());

	TechnicianFactory _technicianFactory;
	_robotFactory = &_technicianFactory;
	add(_robotFactory->create());
	add(_robotFactory->create());
}

RobotController::~RobotController()
{
	for (auto &robotList : vRobotList) {
		delete robotList;
	}

	vRobotList.clear();
}

void RobotController::add(Robot *_RobotPtr)
{
	vRobotList.push_back(_RobotPtr);
}


Robot* RobotController::lookForRobot(enumRobotType _type)
{
	while (true)
	{
		for (auto &robot : vRobotList) {
			if (robot->is() == _type && !robot->isBusy())
				return robot;
		}
	}
}

void RobotController::listenForRequests()
{
	while (true)
	{
		this->doMaintenance();

		Response responseCommand = resourceManager->getRobotCommands();
		json::value jsonTasks = responseCommand.getResponse();

		for (int i = 0; i < jsonTasks.size(); i++)
			doRequest(jsonTasks[i]);

		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	}
}

void RobotController::doMaintenance() {
	for (auto &robot: vRobotList) {
		if (robot->batteryStatus() < 30) robot->chargeBatteryProcess();
		if (robot->damageStatus() > 70) robot->repairProcess();
	}
}

void RobotController::doRequest(json::value _jsonTask)
{
	std::string _taskID = std::to_string(_jsonTask[U("command")].as_integer());

	Robot *robot = nullptr;

	switch (_taskID[0])
	{
	case '2':
		robot = lookForRobot(TECHNICIAN);
		break;

	case '4':
		robot = lookForRobot(MAID);
		break;

	case '5':
		robot = lookForRobot(COOK);
		break;

	  default:
	    throw ERROR_UNKOWN_TASK;
	}

	robot->setIsBusy(true);
	robot->order(_jsonTask);

	resourceManager->putDoneRobotCommand(_jsonTask[U("command")]);

	logTask(robot, _jsonTask);
	robot->setIsBusy(false);
}

void RobotController::run()
{
	try {
		listenForRequests();
	} catch(const int &error_code){
		std::cout << "Err: " << error_code << std::endl;

		throw error_code;
	}
}

void RobotController::logTask(Robot *robot, json::value _jsonTask) {
	std::cout << "Robot: #" <<  robot->getNumber() << " - (T / B / D) - " <<
                  robot->getRobotType() << "/" <<
                  robot->getBatteryLvl() << "/" <<
                  robot->getDamageLvl() <<
                  std::endl;
	std::wcout << "Task done:" << _jsonTask << std::endl;

	utility::ofstream_t logFile;
	logFile.open("./log.txt", std::ios::app);

	if (!logFile.is_open())
	{
		throw ERROR_LOG_FILE_OPEN_ERROR;
	}

	logFile << _jsonTask;
	logFile.close();
}
