#include <thread>
#include "Robot.h"

using namespace Aipo;

int Robot::RobotCounter = 0;

Robot::Robot()
{
	RobotCounter++;

	robotNumber = RobotCounter;
	batteryLvl = 100;
	damageLvl = 0;
	busy = false;
}

Robot::~Robot()
{
	RobotCounter--;
}

int Robot::getNumber()
{
	return robotNumber;
}

bool Robot::isBusy()
{
	return busy;
}

enumRobotType Robot::is()
{
	 return robotType;
}

void Robot::chargeBatteryProcess()
{
	busy = true;

	std::cout << "Ladowanie Robota: " << robotNumber << std::endl;

	while (batteryLvl < 100)
	{
		batteryLvl += 10;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	if (batteryLvl > 100) batteryLvl = 100;

	busy = false;
}

void Robot::repairProcess()
{
	busy = true;

	std::cout << "Naprawianie Robota: " << robotNumber << std::endl;

	while (damageLvl > 0)
	{
		damageLvl -= 10;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	if (damageLvl < 0) damageLvl = 0;

	busy = false;
}

int Robot::batteryStatus()
{
	return batteryLvl;
}

int Robot::damageStatus()
{
	return damageLvl;
}

enumRobotType Robot::getRobotType() const {
	return robotType;
}
int Robot::getBatteryLvl() const {
	return batteryLvl;
}
int Robot::getDamageLvl() const {
	return damageLvl;
}
void Robot::setIsBusy(bool busy) {
	this->busy = busy;
}


