#pragma once

#include "IConfig.h"
#include "resourceManager.h"
#include "RoomController.h"
#include "RobotController.h"
#include "ThreadPool.h"

namespace Aipo
{
	class Bootstrap
	{
		IConfig *iConfig;
		ResourceManager *resourceManager;
		std::vector <std::shared_ptr<HotelController>> hotelController;
		ThreadPool *threadPool;

	public:
		/**
		 * @brief Construct a new Bootstrap object
		 * 
		 * @param _iCfg 
		 */
		explicit Bootstrap(IConfig *_iCfg);

		/**
		 * @brief Destroy the Bootstrap object
		 * 
		 */
		~Bootstrap();

		/**
		 * @brief 
		 * 
		 */
		void run();
	};
}