#pragma once

#include <vector>
#include <thread>
#include "HotelController.h"

namespace Aipo
{
	class ThreadPool
	{
		std::vector <std::thread> vThread;
	public:
		/**
		 * @brief Construct a new Thread Pool object
		 * 
		 */
		ThreadPool();

		/**
		 * @brief Destroy the Thread Pool object
		 * 
		 */
		~ThreadPool();

		/**
		 * @brief 
		 * 
		 * @param _hotelController 
		 */
		void spawnThread(std::shared_ptr<HotelController> _hotelController);

		/**
		 * @brief 
		 * 
		 */
	  	void join();
	};
}