#include "RobotCook.h"

using namespace Aipo;

Cook::Cook()
{
	robotType = COOK;
}

Cook::Cook(int _robotNumber)
{
	robotType = COOK;
	robotNumber = _robotNumber;
}
void Cook::order(web::json::value _jsonTask) {
	MT.lock();
	busy = true;

	bool isReady = false;

	time_t start = clock();
	while (!isReady)
	{
		if (clock() - start > 6000)
		{
			batteryLvl -= 10;
			damageLvl += 7;
			isReady = true;
		}
	}

	busy = false;
	MT.unlock();
}