#pragma once

/** File errors **/
#define ERROR_FILE_OPEN_ERROR 1
#define ERROR_LOG_FILE_OPEN_ERROR 11

/** API errors **/
#define ERROR_API_LOGIN 2

#define ERROR_UNKOWN_TASK 3

/** STH is busy **/
#define IS_BUSY 3