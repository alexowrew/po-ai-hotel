#pragma once

#include "RobotBuilder.h"
#include "RobotMaid.h"

namespace Aipo
{

	class MaidFactory : public RobotFactory
	{

	public:
		/**
		 * @brief 
		 * 
		 * @return Robot* 
		 */
		virtual Robot* create()
		{
			return new Maid;
		}
	};

}