#pragma once

#include "Robot.h"

namespace Aipo
{
	class RobotFactory
	{
	public:
		/**
		 * @brief 
		 * 
		 * @return Robot* 
		 */
		virtual Robot* create() = 0;
	};

}