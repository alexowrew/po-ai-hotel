#pragma once

#include "IConfig.h"

namespace Aipo
{
	class JSONConfig : public IConfig
	{
		json::value config;

	public:
		/**
		 * @brief Construct a new JSONConfig object
		 * 
		 * @param _path 
		 */
		JSONConfig(utility::string_t _path);
		
		/**
		 * @brief Destroy the JSONConfig object
		 * 
		 */
		~JSONConfig();

		/**
		 * @brief 
		 * 
		 * @param configKey 
		 * @return utility::string_t 
		 */
		virtual utility::string_t get(utility::string_t configKey);
	};
}
