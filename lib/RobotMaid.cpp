#include "RobotMaid.h"

using namespace Aipo;


Maid::Maid()
{
	robotType = MAID;
}

Maid::Maid(int _robotNumber)
{
	robotType = MAID;
	robotNumber = _robotNumber;
}

void Maid::order(web::json::value _jsonTask) {
	MT.lock();
	busy = true;

	bool isReady = false;

	time_t start = clock();
	while (!isReady)
	{
		if (clock() - start > 6000)
		{
			batteryLvl -= 20;
			damageLvl += 10;
			isReady = true;
		}
	}

	busy = false;
	MT.unlock();
}