#pragma once

#include "definitions.h"
#include "resourceManager.h"

#include <ctime>
#include <string>
#include <cpprest/json.h>
#include <mutex>
#include <thread>
#include <chrono>

namespace Aipo
{
	enum enumRobotType { MAID = 0, COOK = 1, TECHNICIAN = 2 };

	class Robot
	{
		static int RobotCounter;

	protected:

		std::mutex MT;
		enumRobotType robotType;
		int robotNumber;
		int batteryLvl;
		int damageLvl;
		bool busy;
     public:
      enumRobotType getRobotType() const;
      int getBatteryLvl() const;
      int getDamageLvl() const;

		/**
		 * @brief Construct a new Robot object
		 * 
		 */
		Robot();

		/**
		 * @brief Destroy the Robot object
		 * 
		 */
		virtual ~Robot();

		/**
		 * @brief Get the Number object
		 * 
		 * @return int 
		 */
		int getNumber();

		/**
		 * @brief 
		 * 
		 * @return true 
		 * @return false 
		 */
		bool isBusy();

		/**
		 * @brief
		 *
		 * @return void
		 */
		void setIsBusy(bool);

		/**
		 * @brief 
		 * 
		 * @return enumRobotType 
		 */
		enumRobotType is();

		/**
		 * @brief 
		 * 
		 * @return int 
		 */
		int batteryStatus();

		/**
		 * @brief 
		 * 
		 */
		void chargeBatteryProcess();

		/**
		 * @brief 
		 * 
		 * @return int 
		 */
		int damageStatus();

		/**
		 * @brief 
		 * 
		 */
		void repairProcess();

		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 * @param _resourceManager 
		 */
		virtual void order(web::json::value _jsonTask) = 0;
	};
}