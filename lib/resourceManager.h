#pragma once

#include <string>
#include "client.h"
#include "response.h"
#include <mutex>


namespace Aipo {

	class ResourceManager {

		std::shared_ptr<Client> client;
		utility::string_t authorizationToken;
		std::mutex MT;

	public:
		/**
		 * @brief Construct a new Resource Manager object
		 * 
		 * @param client 
		 */
		explicit ResourceManager(Client *client);

		/**
		 * @brief 
		 * 
		 * @param username 
		 * @param password 
		 * @return Response 
		 */
		Response login(utility::string_t username, utility::string_t password);

		/**
		 * @brief Get the Available Rooms object
		 * 
		 * @return Response 
		 */
		Response getAvailableRooms();

		/**
		 * @brief Get the Robot Commands object
		 * 
		 * @return Response 
		 */
		Response getRobotCommands();

		/**
		 * @brief 
		 * 
		 * @param _jsonValue 
		 */
		Response putDoneRobotCommand(json::value _jsonValue);

		/**
		 * @brief Set the Authorization Token object
		 * 
		 * @param token 
		 */
		void setAuthorizationToken(utility::string_t token);

		/**
		 * @brief Destroy the Resource Manager object
		 * 
		 */
		~ResourceManager();
	};

}