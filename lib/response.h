#pragma once

#include <cpprest/json.h>

namespace Aipo {

	class Response {
		int statusCode;
		web::json::value responseObject;

	public:
	  	Response(int statusCode);

		/**
		 * @brief Construct a new Response object
		 * 
		 * @param statusCode 
		 * @param responseObject 
		 */
		Response(int statusCode, const web::json::value &responseObject);

		/**
		 * @brief Get the Response object
		 * 
		 * @return web::json::value 
		 */
		web::json::value getResponse();
	};

}
