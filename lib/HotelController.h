#pragma once

#include "resourceManager.h"
#include "IConfig.h"

namespace Aipo
{
	class HotelController
	{

	protected:
		ResourceManager *resourceManager; // przekazana jako parametr - nie nalezy usuwac, dzieje sie to w Bootstrapie
		Response *responseToken;			// przechowuje info o tokenie
		IConfig *config;					// przechowuje config.json - nie jest tworzony jako new 

	public:
		/**
		 * @brief Construct a new Hotel Controller object
		 * 
		 * @param resourceManager 
		 * @param config 
		 */
		HotelController(ResourceManager *resourceManager, IConfig *config);
		
		/**
		 * @brief Destroy the Hotel Controller object
		 * 
		 */
		virtual ~HotelController();

		/**
		 * @brief 
		 * 
		 * @param login 
		 * @param password 
		 */
		void login(utility::string_t login, utility::string_t password);
		
		/**
		 * @brief 
		 * 
		 */
		virtual void run() = 0;
	};

}

