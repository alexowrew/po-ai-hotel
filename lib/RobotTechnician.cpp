#include "RobotTechnician.h"

using namespace Aipo;

Technician::Technician()
{
	robotType = TECHNICIAN;
}

Technician::Technician(int _robotNumber)
{
	robotType = TECHNICIAN;
	robotNumber = _robotNumber;
}
void Technician::order(web::json::value _jsonTask) {
	MT.lock();
	busy = true;

	bool isReady = false;

	// get room and device from jsonTask

	time_t start = clock();
	while (!isReady)
	{
		if (clock() - start > 6000)
		{
			batteryLvl -= 30;
			damageLvl += 20;
			isReady = true;
		}
	}

	busy = false;
	MT.unlock();
}