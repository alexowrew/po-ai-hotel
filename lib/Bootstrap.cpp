#include "Bootstrap.h"


using namespace Aipo;

Bootstrap::Bootstrap(IConfig *_iCfg) : iConfig(_iCfg)
{
	resourceManager = new ResourceManager(new Aipo::Client(iConfig->get(U("api_url"))));
	threadPool = new ThreadPool;
}

Bootstrap::~Bootstrap()
{
	delete resourceManager;
	delete threadPool;
}

void Bootstrap::run()
{
	auto roomController = std::make_shared<RoomController>(new ResourceManager(new Aipo::Client(iConfig->get(U("api_url")))), iConfig);

	hotelController.push_back(roomController);
	threadPool->spawnThread(hotelController[0]);

	auto robotController = std::make_shared<RobotController>(new ResourceManager(new Aipo::Client(iConfig->get(U("api_url")))), iConfig);

	hotelController.push_back(robotController);
	threadPool->spawnThread(hotelController[1]);

	threadPool->join();
}