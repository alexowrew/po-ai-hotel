#include "ThreadPool.h"

using namespace Aipo;

ThreadPool::ThreadPool() = default;

ThreadPool::~ThreadPool()
{
}

void ThreadPool::spawnThread(std::shared_ptr<HotelController> _hotelController)
{
	vThread.push_back(std::thread(&HotelController::run, _hotelController));
}

void ThreadPool::join() {
	for (auto &thread : vThread) {
		thread.join();
	}
}
