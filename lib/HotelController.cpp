#include "HotelController.h"

Aipo::HotelController::HotelController(Aipo::ResourceManager *resourceManager, Aipo::IConfig *config) : resourceManager(
    resourceManager), config(config) {}

Aipo::HotelController::~HotelController()
{
	delete responseToken;
}

void Aipo::HotelController::login(utility::string_t login, utility::string_t password) {
	responseToken = new Response(resourceManager->login(login, password));

  if (!responseToken->getResponse().has_field(U("token"))) {
    throw ERROR_API_LOGIN;
  }

  resourceManager->setAuthorizationToken(responseToken->getResponse()[U("token")].as_string());
}
