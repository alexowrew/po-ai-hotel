#pragma once

#include "Robot.h"

namespace Aipo
{
	class Maid : public Robot
	{
	public:
		/**
		 * @brief Construct a new Maid object
		 * 
		 */
		Maid();

		/**
		 * @brief Construct a new Maid object
		 * 
		 * @param _robotNumber 
		 */
		Maid(int _robotNumber);

		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 * @param _resourceManager 
		 */
		virtual void order(web::json::value _jsonTask);
	};
}