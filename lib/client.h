#pragma once

#include <cpprest/http_client.h>
#include "response.h"

using namespace web;
using namespace web::http;
using namespace web::http::client;

namespace Aipo {

	class Client {
		std::shared_ptr<web::http::client::http_client> client;

	public:
		/**
		 * @brief Construct a new Client object
		 * 
		 * @param config 
		 */
        explicit Client(utility::string_t config);
		
		/**
		 * @brief Destroy the Client object
		 * 
		 */
		~Client();

		/**
		 * @brief 
		 * 
		 * @param request 
		 * @return http_response 
		 */
		Response request(http_request &request);

		/**
		 * @brief 
		 * 
		 * @param request 
		 * @param authorizationToken 
		 * @return http_response 
		 */
		Response requestAuthorized(http_request &request, utility::string_t &authorizationToken);
	};

}
