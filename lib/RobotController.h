#pragma once

#include <vector>
#include <thread>
#include <string>
#include <mutex>
#include <chrono>
#include <thread>

#include "HotelController.h"
#include "MaidFactory.h"
#include "TechnicianFactory.h"
#include "CookFactory.h"

namespace Aipo
{
	class RobotController : public HotelController
	{
		std::mutex MT;

		std::vector<Robot*> vRobotList;	//lista wskaznikow na utworzone roboty

		/**
		 * @brief 
		 * 
		 */
		void listenForRequests();

		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 */
		void doRequest(json::value _jsonTask);

	public:
		/**
		 * @brief Construct a new Robot Controller object
		 * 
		 * @param resourceManager 
		 * @param config 
		 */
		RobotController(ResourceManager *resourceManager, IConfig *config);

		/**
		 * @brief Destroy the Robot Controller object
		 * 
		 */
		~RobotController();

		/**
		 * @brief 
		 * 
		 * @param _RobotPtr 
		 */
		void add(Robot *_RobotPtr);

		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 */
		void logTask(Robot *robot, json::value _jsonTask);


		/**
		 * @brief 
		 * 
		 * @param _type 
		 * @return Robot* 
		 */
		Robot* lookForRobot(enumRobotType _type);

		/**
		 * @brief 
		 * 
		 */
		void run() override;

		/**
		 * @brief 
		 * 
		 */
		void doMaintenance();
	};
}