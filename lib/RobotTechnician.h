#pragma once

#include "Robot.h"

namespace Aipo
{
	class Technician : public Robot
	{

	public:
		/**
		 * @brief Construct a new Technician object
		 * 
		 */
		Technician();

		/**
		 * @brief Construct a new Technician object
		 * 
		 * @param _robotNumber 
		 */
		Technician(int _robotNumber);

		/**
		 * @brief 
		 * 
		 * @param _jsonTask 
		 * @param _resourceManager 
		 */
		void order(web::json::value _jsonTask) override;
	};
}