// standard libraries
#include <iostream>
#include "lib/Bootstrap.h"
#include "lib/JSONConfig.h"

int main() {

	Aipo::JSONConfig jsonConfig(U("./config.json"));
	Aipo::Bootstrap bootStrap(&jsonConfig);

	try{
		bootStrap.run();
	} catch(int error_code){
		std::cout << "Error! (" << error_code  << ")" << std::endl;
	}


//	while (true); //wylaczyc mozna przez X (prawy gorny rog)

	return 0;
}